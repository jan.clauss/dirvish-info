<?php

	// ####################################################
	// Dirvish
	// ####################################################

	$dirvish_masterconfig='/etc/dirvish/master.conf';

	// ####################################################
	// Composer
	// ####################################################
	
	// Pfad zur Composer-Installation vendor/autoload.php
	$composer_vendorautoload='';
	
	// ####################################################
	// Google Sheets
	//
	// https://developers.google.com/sheets/api/quickstart/php
	//
	// ####################################################
	
	$sheets_authConfig='';
	$sheets_tokenPath='';
	$sheet_id=''; // Entweder der Komplette Link oder die ID
	$sheet_name='rawdata'; // Seite (Sheet) des Spreadsheets. Wenn nicht gesetz, dann erste Seite
	$sheets_usermail=''; // Nur als Info, für wen freigegeben werden muss

	$block_size='M'; // Blocksize für die Ausgabe der Dateisysteminformationen ins Sheet - entspricht --block-size von df (K, M, G, T, P, E, Z, Y, KB, MB...)
	
	// ####################################################
	// BOT
	// ####################################################
	
	// Daten vom @botfather
	$bot_name="clauss-halle.de";
	$bot_benutzername="clausshalledebot";
	$bot_token="geheim";
	
	$bot_sendids=array(123456789);

	// Die Domain des Servers
	$bot_server="clauss-halle.de";
	// Nur eine Vereinfachung und Gewohnheit
//	$bot_db=$database_table_main;
?>