#!/usr/bin/php
<?php
	include "../hiddenvariables.dirvishinfo.php.inc";

	include_once "class/Autoloaderclass.php.inc";

	if($composer_vendorautoload<>'') {
		require $composer_vendorautoload;
	}

	$master = new dirvishmaster($dirvish_masterconfig);
//	print_r($master);

	$sheetarray=array(); // Array in dem die Zusammenfassung für das Sheet gesammelt werden
	$sheetarray[]=array('name' => 'TimeStamp', 'value' => time());
	$sheetarray[]=array('name' => 'Date', 'value' => date('Y-m-d',time()));
	$sheetarray[]=array('name' => 'Time', 'value' => date('H:i:s',time()));
	
	
	function kommasersetzen($str) {
		$str=preg_replace('/,/','',$str);
		$str=preg_replace('/\./',',',$str);
		preg_match('/([-0-9,]+)/',$str,$arr);
		$str=$arr[1];
		return($str);
	}

	// **************************************************
	// Brancheinfoss in $allbranches laden
	// **************************************************

	foreach($master->branches AS $branchename => $backuptime) {
		$branche = new dirvishbackup($master->bank, $branchename);
//		print_r($branche);
//		print_r($branche->backupinfo($branche->last));
		$allbranches[]=$branche->backupinfo($branche->last);
		$branche=null;
	}
	
	// **************************************************
	// Ausgabe der Branche-Liste- Transfer
	// **************************************************
	
	$infotext=date('Y-m-d H:i:s')."\n";
	$infotext.="Branche  Transfer-Time(s)  -Files         -Size\n";
	foreach($allbranches AS $info) {
		$infotext.=str_pad($info['branchename'], 16, ' ').' '.str_pad($info['summary']['time'], 7, ' ', STR_PAD_LEFT).' '.str_pad($info['numberoftransferedfiles'], 8, ' ', STR_PAD_LEFT).' '.str_pad($info['totaltransferedfilesize'],13,' ', STR_PAD_LEFT);
		if($info['summary']['status'] == 'success') $infotext.='✔'; else $infotext.='❌';
		if($info['backup'] < date('Ymd')) {
			$infotext.='❗ '.substr($info['backup'],0,4).'-'.substr($info['backup'],4,2).'-'.substr($info['backup'],6,2);
		}
		$infotext.="\n";
		$sheetarray[]=array('name' => 'Time '.$info['branchename'], 'value' => $info['summary']['time']);
		$sheetarray[]=array('name' => 'Files '.$info['branchename'], 'value' => kommasersetzen($info['numberoftransferedfiles']));
		$sheetarray[]=array('name' => 'FileSize '.$info['branchename'], 'value' => kommasersetzen($info['totaltransferedfilesize']));
	}
//	echo $infotext;
	$tg=new telegram($bot_token);
	if(is_object($tg)) {
		foreach($bot_sendids AS $user_id) {
			$tg->sendmessage($user_id,'<code>'.$infotext.'</code>');
		}
	}
	
	// **************************************************
	// Ausgabe der Branche-Liste- Absolut
	// **************************************************
	
	$infotext="Branche                   Size\n";
	$gesamtsize=0;
	foreach($allbranches AS $info) {
		$gesamtsize=$gesamtsize+intval(preg_replace('/,/','',$info['totalfilesize']));
	}
	foreach($allbranches AS $info) {
		$prozent=number_format(intval(preg_replace('/,/','',$info['totalfilesize']))/$gesamtsize*100,1);
		$infotext.=str_pad($info['branchename'], 16, ' ').' '.str_pad($info['totalfilesize'],13,' ', STR_PAD_LEFT).' '.str_pad($prozent.' %',13,' ', STR_PAD_LEFT);
		$infotext.="\n";
		$sheetarray[]=array('name' => 'TotalFileSize '.$info['branchename'], 'value' => kommasersetzen($info['totalfilesize']));
		$sheetarray[]=array('name' => 'TotalPercent '.$info['branchename'], 'value' => kommasersetzen($prozent));
	}
//	echo $infotext;
	$tg=new telegram($bot_token);
	if(is_object($tg)) {
		foreach($bot_sendids AS $user_id) {
			$tg->sendmessage($user_id,'<code>'.$infotext.'</code>');
		}
	}

	// **************************************************
	// Plattenbelegung
	// **************************************************
	$infotext="Dateisystem     Größe    Benutzt     Frei     Ben.% Mountpoint\n";
	exec('df -h',$output,$errcode);
	foreach($output AS $oline) {
		if(preg_match('/([a-z0-9\/]+)\s+([0-9,]+[yzeptgmk]?)\s+([0-9,]+[yzeptgmk]?)\s+([0-9,]+[yzeptgmk]?)\s+([0-9,]+%)\s([a-z0-9\/]+)/i',$oline,$arr)) {
			$infotext.=str_pad($arr[1], 10, ' ').' '.str_pad($arr[2], 10, ' ', STR_PAD_LEFT).' '.str_pad($arr[3], 10, ' ', STR_PAD_LEFT).' '.str_pad($arr[4], 10, ' ', STR_PAD_LEFT).' '.str_pad($arr[5], 7, ' ', STR_PAD_LEFT).' '.str_pad($arr[6], 10, ' ');
			$infotext.="\n";
		}
	}
	unset($output);
	exec("df -B$block_size",$output,$errcode);
	foreach($output AS $oline) {
		if(preg_match('/([a-z0-9\/]+)\s+([0-9,]+[yzeptgmk]?)\s+([0-9,]+[yzeptgmk]?)\s+([0-9,]+[yzeptgmk]?)\s+([0-9,]+%)\s([a-z0-9\/]+)/i',$oline,$arr)) {
//			print_r($arr);
			$filesystemname=$arr[1];
			$filesystemindex='';
			while(IsSet($filesystems[$filesystemname.$filesystemindex])) {
				$filesystemindex=intval($filesystemindex)+1;
			}
			$filesystemname.=$filesystemindex;
			$filesystems[$filesystemname]=true;
			$sheetarray[]=array('name' => 'FS size '.$filesystemname, 'value' => kommasersetzen($arr[2])); // Größe
			$sheetarray[]=array('name' => 'FS used '.$filesystemname, 'value' => kommasersetzen($arr[3])); // Benutzt
			$sheetarray[]=array('name' => 'FS free '.$filesystemname, 'value' => kommasersetzen($arr[4])); // Frei
			$sheetarray[]=array('name' => 'FS percent '.$filesystemname, 'value' => kommasersetzen($arr[5])); // Benutzt
			$sheetarray[]=array('name' => 'FS mountpoint '.$filesystemname, 'value' => $arr[6]); // MountPoint
		}
	}
//	print_r($sheetarray);
//	echo $infotext;
	$tg=new telegram($bot_token);
	if(is_object($tg)) {
		foreach($bot_sendids AS $user_id) {
			$tg->sendmessage($user_id,'<code>'.$infotext.'</code>');
		}
	}
	
	if($composer_vendorautoload<>'---' and $sheets_authConfig<>'' and $sheets_tokenPath<>'' and $sheet_id<>'') { // daten in Sheet schreiben
//		print_r($sheetarray);
		if(preg_match('#/spreadsheets/d/([-a-zA-Z0-9_]+)#',$sheet_id,$arr)) { // SheetId als Link gespeichert
			$sheet_id=$arr[1];

			$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
			$range='A1:ZZ';
			if(IsSet($sheet_name) and trim($sheet_name<>'')) $range=$sheet_name."!$range";
			$result=$sheets->appendByNames($sheet_id, $range, $sheetarray);
			if($result == false) { // Fehler beim schreiben aufgetreten
				$tg->sendmessage($user_id, 'Fehler beim schreiben ins Sheet');
			}else{
				if(IsSet($result['updateValues']['updatedRows'])) {
					$tg->sendmessage($user_id, "✔ ".$result['updateValues']['updatedRows'].' Zeilen ins Sheet geschrieben');
				}
			}
		}
	}
 
?>