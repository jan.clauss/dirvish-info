<?php

/**
* Klasse für den die master.conf-Datei von Dirvish
*
*	@author Jan Clauß
* @version 1.0
* 
*
* Wertet die master.conf-Datei von Dirvish aus und gibt den Backup-Pfad und die Branches zurück
*/

class dirvishmaster
{
	public $bank=''; // Der Pfad zu den Backups
	public $exclude=array(); // Exclusion-Pfade
	public $branches=array(); // die einzelnen Branches $bankname => $time)
	public $expiredefault=''; 
	public $expirerule=array(); //
	public $preserver='';
	public $postserver='';
	public $speedlimit='';

	/**
	* Constructor
	*
	* @param string $masterconf Pfad und Name zur master.conf
	*/
	function __construct($masterconf)
	{
		// Datei öffnen
		if(!$conf=fopen($masterconf,'r')) {
			error_log(__FILE__.', '.__LINE__."Konnte $masterconf nicht lesen");
			$return=false;
		}
		
		$lastmain='';
		
		// Zeilenweise lesen
		while(($line=fgets($conf)) !== false ) {
			if($line[0] <> '#' and trim($line) <> '') { // Kommentare überspringen
				if(preg_match('/^([-a-z0-9]+):(.*)/i',$line,$arr)) {
					$lastmain=strtolower($arr[1]);
					$line=$arr[2];
				}
				switch($lastmain) {
					case 'bank':
						if(preg_match('/^\s+([-a-z0-9_\/]+)/i',$line,$arr)) {
							$this->bank=trim($arr[1]);
						}
					break;
					case 'exclude':
						if(preg_match('/^\s+(.*)/',$line,$arr)) {
							$this->exclude[]=trim($arr[1]);
						}
					break;
					case 'runall':
						if(preg_match('/^\s+([-a-z0-9_]+)\s+(\d\d:\d\d)/i',$line,$arr)) {
							$this->branches[$arr[1]]=trim($arr[2]);
						}
					break;
					case 'expire-default':
						if(preg_match('/^\s+(.*)/',$line,$arr)) {
							$this->expiredefault=trim($arr[1]);
						}
					break;
					case 'expire-rule':
						if(preg_match('/^\s+(.*?)\s+(.*?)\s+(.*?)\s+(.*?)\s+(.*?)\s+(.*)/',$line,$arr)) {
							$this->expirerule[]=array(
								'MIN' => $arr[1],
								'HR' => $arr[2],
								'DOM' => $arr[3],
								'MON' => $arr[4],
								'DOW' => $arr[5],
								'STRFTIME_FMT' => $arr[6]
							);
						}
					break;
					case 'pre-server':
						if(preg_match('/^\s+([-a-z0-9_\/]+)/i',$line,$arr)) {
							$this->preserver=trim($arr[1]);
						}
					break;
					case 'post-server':
						if(preg_match('/^\s+([-a-z0-9_\/]+)/i',$line,$arr)) {
							$this->postserver=trim($arr[1]);
						}
					break;
					case 'speed-limit':
						$this->speedlimit=intval($line);
					break;
				}
			}
		}
  }
  
}

?>