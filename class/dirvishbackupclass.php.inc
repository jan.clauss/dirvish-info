<?php

/**
* Klasse für den die master.conf-Datei von Dirvish
*
*	@author Jan Clauß
* @version 1.0
* 
*
* Wertet die master.conf-Datei von Dirvish aus und gibt den Backup-Pfad und die Branches zurück
*/

class dirvishbackup
{
	public $path=''; // Der Pfad zum Backup
	public $branche='';
	public $backups=array();
	public $last=''; // Letztes Backup

	/**
	* Constructor
	*
	* @param string $masterconf Pfad und Name zur master.conf
	*/
	function __construct($bank, $branche)
	{
		$dir="$bank/$branche";
		// Prüfen, ob Pfad existiert
		if(!is_dir($dir)) {
			error_log(__FILE__.', '.__LINE__."  $dir is no directory");
			return(false);
		}
		$this->branche=$branche;
		$lastfile=''; // Enthält das neueste Backup
		if($handle=opendir($dir)) {
			while(($file = readdir($handle)) !== false) {
				if(filetype($dir.'/'.$file) == 'dir') { // Ist Directory
					if(preg_match('/^\d{8}$/',$file)) { // Ist ein Unterverzeichnis mit Datum als Name
						$this->backups[$file]=$dir.'/'.$file;
						if($file > $lastfile) { // Backup ist neuer als das in $lastfile
							$lastfile=$file;
						}
					}
				}
			}
		}
		$this->last=$lastfile;
	}
  
	public function backupinfo($backup)
	{
		$returnarray=array();
		$backuppath=$this->backups[$backup];
		$backuplog="$backuppath/log";
		$backupsummary="$backuppath/summary";
		$returnarray['backuppath']=$backuppath;

		$returnarray['backup']=$backup;
		$returnarray['branchename']=$this->branche;
		
		$returnarray['backuplog']=$backuplog;
		// Log-Datei öffnen
		if(!$log=fopen($backuplog,'r')) {
			error_log(__FILE__.', '.__LINE__."Konnte $backuplog nicht lesen");
			$return=false;
		}
		// Zeilenweise lesen
		while(($line=fgets($log)) !== false ) {
			if(preg_match('/^Number of files: (\d+) \(reg: (\d+), dir: (\d+)\)/', $line, $arr)) {
				$returnarray['numberoffiles']=array('all' => $arr[1], 'reg' => $arr[2], 'dir' => $arr[3]);
			}elseif(preg_match('/^Number of created files: (\d+)/', $line, $arr)) {
				$returnarray['numberofcreatedfiles']=$arr[1];
			}elseif(preg_match('/^Number of regular files transferred: (\d+)/', $line, $arr)) {
				$returnarray['numberoftransferedfiles']=$arr[1];
			}elseif(preg_match('/^Total file size: ([0-9,]+) bytes/', $line, $arr)) {
				$returnarray['totalfilesize']=$arr[1];
			}elseif(preg_match('/^Total transferred file size: ([0-9,]+) bytes/', $line, $arr)) {
				$returnarray['totaltransferedfilesize']=$arr[1];
			}elseif(preg_match('/^Literal data: ([0-9,]+) bytes/', $line, $arr)) {
				$returnarray['literaldata']=$arr[1];
			}elseif(preg_match('/^Matched data: ([0-9,]+) bytes/', $line, $arr)) {
				$returnarray['matcheddata']=$arr[1];
			}elseif(preg_match('/^File list size: ([0-9,]+)/', $line, $arr)) {
				$returnarray['filelistsize']=$arr[1];
			}elseif(preg_match('/^File list generation time: ([0-9,.]+) seconds/', $line, $arr)) {
				$returnarray['filelistgenerationtime']=$arr[1];
			}elseif(preg_match('/^File list transfer time: ([0-9,.]+) seconds/', $line, $arr)) {
				$returnarray['filelisttransferetime']=$arr[1];
			}elseif(preg_match('/^Total bytes sent: ([0-9,]+)/', $line, $arr)) {
				$returnarray['totalbytessend']=$arr[1];
			}elseif(preg_match('/^Total bytes received: ([0-9,]+)/', $line, $arr)) {
				$returnarray['totalbytesreceived']=$arr[1];
			}elseif(preg_match('/^sent ([0-9,]+) bytes  received ([0-9,]+) bytes  ([0-9,.]+) bytes\/sec/', $line, $arr)) {
				$returnarray['transfer']=array('sent' => $arr[1], 'received' => $arr[2], 'speed' => $arr[3]);
			}elseif(preg_match('/^total size is ([0-9,]+)  speedup is ([0-9,.]+)/', $line, $arr)) {
				$returnarray['total']=array('size' => $arr[1], 'speedup' => $arr[2]);
			}
		}
		fclose($log);
		
		$returnarray['backupsummary']=$backupsummary;
		// summary-Datei öffnen
		if(!$sum=fopen($backupsummary,'r')) {
			error_log(__FILE__.', '.__LINE__."Konnte $backupsummary nicht lesen");
			$return=false;
		}
		// Zeilenweise lesen
		while(($line=fgets($sum)) !== false ) {
			if(preg_match('/^Backup-begin: (\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d)/', $line, $arr)) {
				$returnarray['summary']['begin'] = $arr[1];
			}elseif(preg_match('/^Backup-complete: (\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d)/', $line, $arr)) {
				$returnarray['summary']['complete'] = $arr[1];
			}elseif(preg_match('/^Status: ([a-z0-9]+)/i', $line, $arr)) {
				$returnarray['summary']['status'] = $arr[1];
			}
		}
		if(IsSet($returnarray['summary']['begin']) and IsSet($returnarray['summary']['complete'])) $returnarray['summary']['time'] = strtotime($returnarray['summary']['complete']) - strtotime($returnarray['summary']['begin']);

		return($returnarray);
	}

}

?>