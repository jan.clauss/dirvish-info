﻿<?php

/**** Klasse für Telegram
*
*	@Author: Jan Clauß
* @Version: 1.0
* 
*/

class telegram
{
	private $bottoken='';

	public $maxmessagesize = 4096; // Telegram kann nur 4096 Byte große Nachrichten
	public $message_limit = 4; // Maximal anzahl Nachrichten, wenn eine Nachricht gesplittet wird
	public $parsemode = 'HTML'; // Telegram wertet die Übergebenen Daten als html aus, nur ein paar der html Formatierungen
	public $webpagepreview = false; // Schaltet die Link-Vorschau aus
	public $delete_not_editmessage = false; // Nachricht wird nicht geändert, sondern gelöscht und neu geschrieben
	
	private $lastpollmessageid=0; // Nur für das Polling, die letzte Message ID
	
	function __construct($bot_token)
	{
  	$this->bottoken=$bot_token;
  }
  
  public function getbottoken() {
  	return($bottoken);
  }
  
  public function installwebhook($script = '', $maxconnections = -1, $allowed_updates = '', $certificate = '')
  // Webhook installieren
  {
		$ch = curl_init();
		if($script == '') { // Kein Script übergeben, nehme laufendes Skript als Webhook
			$script=$_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
		}
  	echo $this->bottoken." - ".$script;
		$data['url'] = $script; // Der Skriptname, der von Telegram aufgerufen wird
		if($certificate <> '') { // !!Ungetestet
			$data['certificate'] = ''; // Das Zertifikat, bei nichtauflösbaren Zertifikaten
		}
		if($maxconnections <> -1) { // !!Ungetestet
			$data['max_connections']=$maxconnections; // Maximal gleichzeitige Verbinungen
		}
		if($allowed_updates <> '') { // !!Ungetestet
			$data['allowed_updates']=''; // Kommandos die erlaubt sind
		}
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/setWebhook");
		$res = curl_exec($ch);
		if(!IsSet($res['ok']) or $res['ok']<>true) { // Fehler ins Error-Log
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'error',$res,'main');
		}
		return($res);
	}

	public function uninstallwebhook()
		// Webhook stoppen
	{
		$ch = curl_init();
		$data['url'] = "";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/setWebhook");
		$res = curl_exec($ch);
		if(!IsSet($res['ok']) or $res['ok']<>true) { // Fehler  ins Error-Log
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'error',$res,'main');
		}
		return($res);
	}
	
	public function pollmessage()
		// Ruft neue Nachrichten ab, wenn kein Webhook installiert ist
	{
		return(false); // Noch nicht implementiert
	}
	
	public function decodemessage($message)
		// Dekodiert eine empfangene Nachricht in ein einheitliches Array (Kommando, Callback, Antwort)

		// $message enthält die Nachtricht von Telegram
		
		//* ['date'] => Zeitpunkt der Nachricht ( timestamp )
		//* ['message_typ'] => query, callback_query, location, document, photo
		//* ['user_id'] => Telegram ID
		//* ['first_name'] => Vorname
		//* ['last_name'] => Nachname
		//* ['user_name'] => Telegram Username
		//* ['message_id'] => ID der Nachricht
		//* ['text'] => Der Text aus der Eingabe
		//* ['command'] => Extrahiertes Kommando, erstes Wort ([a-z0-9]+)
		//* ['args'] => Array aller Argumente
		//* ['lat'] => location - latitude
		//* ['lng'] => location - longitude
		//* ['filename'] => document
		//* ['filesize'] => document, photo
		//* ['filemime'] => document, photo
		//* ['fileid'] => document, photo
		//* ['fileuid'] => document, photo
		//* ['width'] => photo
		//* ['height'] => photo
		//* ['thumb']['filesize'] => document, photo
		//* ['thumb']['fileid'] => document, photo
		//* ['thumb']['fileuid'] => document, photo
		//* ['thumb']['width'] => document, photo
		//* ['thumb']['height'] => document, photo
		//* ['thumb1']['filesize'] => photo
		//* ['thumb1']['fileid'] => photo
		//* ['thumb1']['fileuid'] => photo
		//* ['thumb1']['width'] => photo
		//* ['thumb1']['height'] => photo
	{
		$ret=array();
		$msg=json_decode($message,true);
		// ########################################
		// User ermitteln und prüfen
		// ########################################
		if(IsSet($msg['message']['date'])) $ret['date']=$msg['message']['date'];
		if(IsSet($msg['message']) and IsSet($msg['message']['from']['id'])) {
//		error_log(__FILE__.' '. __LINE__.' '.$message);
			// Normale Message an Bot Prüfen, das ID Vorhanden für Identifikation
			$ret['message_typ']='query';
			$ret['user_id']=$msg['message']['from']['id'];
			$ret['first_name']='';
			if(IsSet($msg['message']['from']['first_name'])) $ret['first_name']=$msg['message']['from']['first_name'];
			$ret['last_name']='';
			if(IsSet($msg['message']['from']['last_name'])) $ret['last_name']=$msg['message']['from']['last_name'];
			$ret['username']='';
			if(IsSet($msg['message']['from']['username'])) $ret['username']=$msg['message']['from']['username'];
			$ret['text']='';
			if(IsSet($msg['message']['text'])) $ret['text']=$msg['message']['text'];
			$ret['message_id']=$msg['message']['message_id'];
			if(IsSet($msg['message']['location'])) {
				// Position
				$ret['message_typ']='location';
				$ret['lat']=$msg['message']['location']['latitude'];
				$ret['lng']=$msg['message']['location']['longitude'];
			}
			if(IsSet($msg['message']['document'])) {
				// Dokument - Liefert z.T. ein Thumbnail mit
				$ret['message_typ']='document';
				$ret['filename']=$msg['message']['document']['file_name'];
				$ret['filesize']=$msg['message']['document']['file_size'];
				$ret['filemime']=$msg['message']['document']['mime_type'];
				$ret['fileid']=$msg['message']['document']['file_id'];
				$ret['fileuid']=$msg['message']['document']['file_unique_id'];
				if(IsSet($msg['message']['document']['thumb'])) {
					$ret['thumb']['filesize']=$msg['message']['document']['thumb']['file_size'];
					$ret['thumb']['fileid']=$msg['message']['document']['thumb']['file_id'];
					$ret['thumb']['fileuid']=$msg['message']['document']['thumb']['file_unique_id'];
					$ret['thumb']['width']=$msg['message']['document']['thumb']['width'];
					$ret['thumb']['height']=$msg['message']['document']['thumb']['height'];
				}
			}
			if(IsSet($msg['message']['photo'])) {
				// Photop, Begrenzte Qualität und zwei Thumbnails
				$ret['message_typ']='photo';
				$ret['filesize'] = $msg['message']['photo'][2]['file_size'];
				$ret['fileid'] = $msg['message']['photo'][2]['file_id'];
				$ret['fileuid'] = $msg['message']['photo'][2]['file_unique_id'];
				$ret['width'] = $msg['message']['photo'][2]['width'];
				$ret['height'] = $msg['message']['photo'][2]['height'];
				$ret['thumb']['filesize']=$msg['message']['photo'][0]['thumb']['file_size'];
				$ret['thumb']['fileid']=$msg['message']['photo'][0]['thumb']['file_id'];
				$ret['thumb']['fileuid']=$msg['message']['photo'][0]['thumb']['file_unique_id'];
				$ret['thumb']['width']=$msg['message']['photo'][0]['thumb']['width'];
				$ret['thumb']['height']=$msg['message']['photo'][0]['thumb']['height'];
				$ret['thumb1']['filesize']=$msg['message']['photo'][1]['thumb']['file_size'];
				$ret['thumb1']['fileid']=$msg['message']['photo'][1]['thumb']['file_id'];
				$ret['thumb1']['fileuid']=$msg['message']['photo'][1]['thumb']['file_unique_id'];
				$ret['thumb1']['width']=$msg['message']['photo'][1]['thumb']['width'];
				$ret['thumb1']['height']=$msg['message']['photo'][1]['thumb']['height'];
			}
		}elseif(IsSet($msg['callback_query']) and IsSet($msg['callback_query']['from']['id'])) {
			// Callback Query, ['data'] wird zu ['text']
			$ret['message_typ']='callback_query';
			$ret['user_id']=$msg['callback_query']['from']['id'];
			$ret['first_name']='';
			if(IsSet($msg['callback_query']['from']['first_name'])) $ret['first_name']=$msg['callback_query']['from']['first_name'];
			$ret['last_name']='';
			if(IsSet($msg['callback_query']['from']['last_name'])) $ret['last_name']=$msg['callback_query']['from']['last_name'];
			$ret['username']='';
			if(IsSet($msg['callback_query']['from']['username'])) $ret['username']=$msg['callback_query']['from']['username'];
			$ret['text']='';
			if(IsSet($msg['callback_query']['data'])) $ret['text']=$msg['callback_query']['data'];
			$ret['message_id']=$msg['callback_query']['message']['message_id'];
		}
		if(IsSet($ret['message_typ'])) {
			if(preg_match('/^(\/?[a-z0-9]+)(.*)/i',$ret['text'],$arr)) {
				$ret['command']=$arr[1];
					$ret['args']=array();
				if(trim($arr[2]) <> '') {
					preg_match_all('/([^ ]+)/',$arr[2], $args);
					$ret['args']=$args[0];
				}
			}
		}
		$ret['originalmessage']=$msg;
		return($ret);
	}
	
	public function sendmessage($user, $message, $othermessagelimit = 0)
		// Sendet eine Nachricht an den User oder Gruppe
		
		// $message -> Message als Text, als Array für Telegram oder 
		//		irgendein anderes Array, das dann in String gewandelt wird, wenn kein ['text'] vorhanden ist
		// $user -> Der Empfänger (Telegram ID)
		// $othermessagelimit -> Maximale Anzahl Nachrichten, die gesendet werden dürfen
		
	{
//		error_log(__FILE__.' '. __LINE__.' '.$message);
	  $res='';
	  $resarr['ok']=true; // Alles in Ornung
		if(is_array($message) and IsSet($message['text'])) { // Array für Telegram
			$messagearray=$message;
			$messagetext=$messagearray['text'];
		}elseif(is_array($message)) { // Irgendein anderes Array, Konvertieren nach String
			$messagetext=print_r($message,true);
		}else{ // Einfacher Text
			$messagetext=$message;
		}
		$messagecount=0;
		if(strlen($messagetext)>$this->maxmessagesize) {
			while(strlen($messagetext) > 0 and ($messagecount < $this->message_limit or $messagecount < $othermessagelimit) and $resarr['ok'] == true) {
				// Teilnachrichten, solange nicht Messagelimit und senden einer Nachricht fehlgeschlagen
				$messagecount++;
				$pos=strrpos(substr($messagetext,0,$this->maxmessagesize),"\n"); // Versuch einen Zeilenumbruch vor dem Message Limit Ende zu finden
				if($pos === false) { // nicht gefunden
					$pos=$this->maxmessagesize;
				}
				$msgtext=substr($messagetext,0,$pos);
				$resarr=$this->sendmessage($user,$msgtext);
				$messagetext=substr($messagetext,$pos); // Gesendeten Teil entfernen
			}
			if(IsSet($messagearray)) {
				$messagearray['text']=substr($msgtext,0,10); // Nur noch Teilstring vom letzten gesendeten Text
				$resarr=$this->sendmessage($user,$messagearray);
			}
		}else{
			$ch = curl_init();
			if(IsSet($messagearray)) {
				$data=$messagearray;
				$data['text']=$messagetext;
			}else{
				$data['text']=$messagetext;
			}
			if(trim($data['text']) == '') {
				return(json_encode(array('ok' => false, 'error_code' => '400', 'description' => 'Bad Request: message text is empty')));
			}
			$data['chat_id'] = intval($user);
			$data['parse_mode'] = $this->parsemode;
			$data['disable_web_page_preview'] = $this->webpagepreview;
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	 		if(IsSet($data['message_id'])) { // Ändern einer Nachricht
	 			if($this->delete_not_editmessage) { // Noch ungetestet
	 				$this->deletemessage($user,$data['message_id']);
	 				curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
	 			}else{
		  		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/editMessageText");
		  	}
	  	}else{
		  	curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
	  	}
			$res = curl_exec($ch);
			$resarr=json_decode($res,true);
			if($resarr['ok']<>true) { // Fehler beim Versenden der Nachricht ins Error-Log
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'error',$res,'main');
				error_log(__FILE__.' '. __LINE__.' '.print_r($resarr,true));
			}
			unset($ch);
		}
		return($resarr);
	}
	
	function testsendstr($user_id,$text) {
		
		//* Sendet einen String Probeweise, um zu testen, ob Fehler enthalten sind
		
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'parse_mode' => 'HTML',
			'disable_web_page_preview'=>true
		); // Schaltet die Link-Vorschau aus
		$data['text']=$text;
		//	error_log(print_r($data,true));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
		$res = curl_exec($ch);
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	
	function deletemessage($user_id,$message_id) {
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'message_id'=>$message_id
		);
//	error_log(__FILE__.' '. __LINE__.' '.print_r($data,true));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  	curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/deleteMessage");
		$res = curl_exec($ch);
		$resarr=json_decode($res,true);
		if($resarr['ok']<>true) { // Fehler beim Versenden der Nachricht ins Error-Log
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'error',$res,'main');
			error_log(__FILE__.' '. __LINE__.' '.print_r($resarr,true));
		}
		unset($ch);
		return($resarr);
	}
	
	public function sendDocument($user_id, $filename, $text, $sendfilename='') 
	// user_id - ID des Users (chat_id)
	// filename - Dateiname mit Pfad der zu sendenden Datei
	// text - Ein mit zu sendender Text
	// $sendfilename - Dateiname, als der die Datei gesendet werden soll
	{
		if(!file_exists($filename)) {
			// Datei nicht vorhanden
			return(array('ok' => 'false', 'description' => 'Datei existiert nicht'));
		}
		if($sendfilename == '') { // Ausgabefilename für Telegram aus $filename ermitteln
			$pos=strrpos($filename,'/');
			if($pos === false) { //Dateiname ohne Pfad
				$sendfilename=$filename;
			}else{
				$sendfilename=substr($filename,$pos+1);
			}
		}
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'text' => $text,
			'document' => new CURLFile($filename,'text/plain',$sendfilename)
		);
		$url="https://api.telegram.org/bot".$this->bottoken."/sendDocument";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit wird die Antwort als String der Funktion curl_exec zurück gegeben
		curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-Type: multipart/form-data"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$res = curl_exec($ch);
		return(json_decode($res,true));
	}
	
	public function getFileName($file_id)
	{
		$ch = curl_init();
		$data=array(
			'file_id'=>$file_id
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/getFile");
		$res = curl_exec($ch);
		return(json_decode($res,true));
	}

	public function getDocument($file_id)
	{
		$filearray=$this->getFileName($file_id);
		if($filearray['ok'] == false) {
			return($filearray);
		}
		$filename="https://api.telegram.org/file/bot".$this->bottoken."/".$filearray['result']['file_path'];
		$ch = curl_init();     
    curl_setopt($ch, CURLOPT_URL, $filename);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($ch, CURLOPT_UPLOAD, 0);  
    $result = curl_exec($ch);  
    curl_close($ch); 
    return(json_decode($result,true));
	}
	
	public function copyDocument($file_id,$localfile) // Bisher Ungetestet
	{
		$local=fopen($localfile,'w');
		if(!$local) {
			return(array('ok' => false, 'error_code' => '2', 'description' => 'File-Error','errorInfo' => "Can't create file"));
		}
		$filearray=$this->getFileName($file_id);
		if($filearray['ok'] == false) {
			return($filearray);
		}
		$filename="https://api.telegram.org/file/bot".$this->bottoken."/".$filearray['result']['file_path'];
		$ch = curl_init();     
    curl_setopt($ch, CURLOPT_URL, $filename);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($ch, CURLOPT_UPLOAD, 0);  
    curl_setopt($ch, CURLOPT_FILE, $local);
    curl_exec($ch);  
    $result=curl_getinfo($ch); // curl_exec liefert auch bei http_code 404 ein true, deswegen über getinfo
    if($result['http_code'] == 200) { // Download erfolgreich
    	$result['ok']=true;
    }else{
    	$result['ok']=false;
    }
    curl_close($ch); 
    return($result);
	}


	
}

?>

